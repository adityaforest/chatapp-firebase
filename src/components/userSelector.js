import React, { useEffect, useState } from 'react'

function UserSelector({ onlineUsers, setshowUserSelector, startNewPrivateChat, username }) {
    const [selectedUser, setSelectedUser] = useState('')

    const selectUser = () => {
        setshowUserSelector(false)
        startNewPrivateChat(selectedUser)
        // console.log(selectedUser)
    }

    return (
        <>
            <div
                className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
            >
                <div className="relative w-auto my-6 mx-auto max-w-3xl">
                    {/*content*/}
                    <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-slate-400 outline-none focus:outline-none"
                        style={{ height: '400px' }}>
                        {/*header*/}
                        <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                            <h3 className="text-3xl font-semibold text-black">
                                Select user to chat with :
                            </h3>
                            <button
                                className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                                onClick={() => (false)}
                            >
                                <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                                    ×
                                </span>
                            </button>
                        </div>
                        {/*body*/}
                        <ul className="w-full text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg dark:bg-gray-700 dark:border-gray-600 dark:text-white overflow-y-auto">
                            {
                                (onlineUsers.length - 1 == 0) ?
                                <h1 className='text-white'>Sorry , no other user online right now ...</h1>
                                :
                                onlineUsers.map((user) => {
                                    if (user !== username) {
                                        return (
                                            <li className="w-full border-b border-gray-200 rounded-t-lg dark:border-gray-600" onClick={e => setSelectedUser(user)}>
                                                <div className="flex items-center pl-3 w-full">
                                                    <input id="list-radio-license" type="radio" value={user.username} name="list-radio" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500" />
                                                    <label for="list-radio-license" className="w-full py-3 ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">{user} </label>
                                                </div>
                                            </li>
                                        )
                                    }                                    
                                })
                            }
                        </ul>
                        {/*footer*/}
                        <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                            <button
                                className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                type="button"
                                onClick={(e) => setshowUserSelector(false)}
                            >
                                Close
                            </button>
                            <button
                                className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                type="button"
                                onClick={(e) => selectUser()}
                            >
                                Select
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
    )
}

export default UserSelector