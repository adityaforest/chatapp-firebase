class TimeManagement {
    getMilisecondsFromDate(date) {
        return date.getTime()
    }

    getTimeDifferenceFromNow(miliseconds) {
        const currentDate = new Date()
        const currentMilliseconds = currentDate.getTime()
        const diffms = currentMilliseconds - miliseconds

        // milisecond template
        const second = 1000
        const minute = second * 60;
        const hour = minute * 60;
        const day = hour * 24;
        const year = day * 365;

        if (diffms < second) {
            return "just now"
        }
        else {
            const resSecond = Math.round(diffms / second)
            if(resSecond / minute < 1){
                return `${resSecond} seconds ago`
            }
            else{
                const resMinute = Math.round(diffms / minute)
                if(resMinute / hour < 1){
                    return `${resMinute} minutes ago`
                }
                else{
                    const resHour = Math.round(diffms / hour)
                    if(resHour / day < 1){
                        return `${resHour} hours ago`
                    }
                    else{
                        const resDay = Math.round(diffms / day)
                        if(resDay / year < 1){
                            return `${resDay} days ago`
                        }
                        else{
                            const resYear = Math.round(diffms / year)
                            if(resYear >= 1){
                                return `${resYear} years ago`
                            }
                        }
                    }
                }
            }
        }

    }
}

export default new TimeManagement()