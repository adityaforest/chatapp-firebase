// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCt6dSUqyk9z7FFHjf5f_JvxW0TvsI-bfk",
  authDomain: "chatapp-firebase-4eee1.firebaseapp.com",
  projectId: "chatapp-firebase-4eee1",
  storageBucket: "chatapp-firebase-4eee1.appspot.com",
  messagingSenderId: "76550973212",
  appId: "1:76550973212:web:4e139862fd0a471523bcd9"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getDatabase(app)