import { onChildAdded, onValue, push, ref, remove, set } from 'firebase/database'
import { useEffect, useState } from 'react'
import { db } from '../firebase'
import UserSelector from '../components/userSelector'
import timeManagement from '@/lib/timeManagement'

export default function Chat({ username, inisial,onlineUsers }) {
    const initChatState = {
        Public: {
            chats: [
                {
                    username: '* DEVELOPER *',
                    message: `Welcome to Forest Chat App ! <br/>
                    Rules : <br/>
                    1. Please be nice to others ! <br/>
                    2. Don't post / send any private information <br/>
                    3. Your chat history will be deleted from the server if you refresh or leave this page <br/><br/>
                    Have a good day ! :)`,
                    time: new Date().getTime().toString()
                }
            ],
            type: 'GroupChat',
            unread: false
        }
    }

    const initRoom = {
        name: 'Public',
        type: 'GroupChat'
    }
    const [chats, setChats] = useState(initChatState)
    const [selectedRoom, setSelectedRoom] = useState(initRoom)
    const [message, setMessage] = useState('')

    const [scrollMaxed, setScrollMaxed] = useState(false)

    const [showUserSelector, setshowUserSelector] = useState(false);

    let userHistory = []

    useEffect(() => {
        //receive groupchat db change
        onChildAdded(ref(db, `/GroupChat/Public`), (snapshot) => {
            if (snapshot.val()) {
                //console.log(snapshot.val())                
                let data = { ...chats }
                data.Public.chats.push(snapshot.val())
                scrollCheck('Public')
                setChats(data)
            }
        })

        //receive privatechat db change , executed one time after each other user first chat
        onChildAdded(ref(db, `/PrivateChat/${username}`), (snapshot) => {
            if (snapshot.val()) {
                scrollCheck(username)
                setChats((prev) => {
                    let data = { ...prev }
                    if ((snapshot.val().username in data) == false) {
                        data = {
                            ...data, [snapshot.val().username]: {
                                chats: [],
                                type: 'PrivateChat',
                                unread: true
                            }
                        }
                    }
                    data[snapshot.val().username].chats.push(snapshot.val())
                    return data
                })
            }
        })

    }, [])

    useEffect(() => {
        console.log(chats)
    }, [chats])


    const scrollCheck = (roomTarget) => {
        let y = document.getElementById('chatContent')
        if (selectedRoom.name === roomTarget) {
            if (Math.ceil(y.scrollTop) > (y.scrollHeight - y.clientHeight - 5) ||
                Math.ceil(y.scrollTop) < (y.scrollHeight - y.clientHeight + 5)) {
                setScrollMaxed(true)
            }
            else {
                setScrollMaxed(false)
            }
        }
    }

    const scrollToBottom = (e) => {
        let y = document.getElementById('chatContent')
        y.scrollTop = (y.scrollHeight - y.clientHeight)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        // console.log(new Date())
        let key = push(ref(db, `/${selectedRoom.type}/${selectedRoom.name}`), {
            username,
            message, 
            time: timeManagement.getMilisecondsFromDate(new Date())                   
        }).key
        userHistory.push(`/${selectedRoom.type}/${selectedRoom.name}/${key}`)

        if(selectedRoom.type == "PrivateChat"){
            setChats((prev) => {
                let data = { ...prev }
                if ((selectedRoom.name in data) == false) {
                    data = {
                        ...data, [selectedRoom.name]: {
                            chats: [],
                            type: 'PrivateChat',
                            unread: true
                        }
                    }
                }
                data[selectedRoom.name].chats.push({
                    username,
                    message,
                    time: timeManagement.getMilisecondsFromDate(new Date())
                })
                return data
            })
        }

        setMessage('')
    }

    useEffect(() => {
        if (scrollMaxed) {
            scrollToBottom()
        }
        window.addEventListener('beforeunload', deleteUser)
        return () => {
            window.addEventListener('beforeunload', deleteUser)
        }
    })

    const deleteUser = (e) => {
        remove(ref(db, `/onlineUsers/${username}`))
        userHistory.forEach(address => remove(ref(db, address)))
    }

    const setshowUserSelectorFunc = (value) => {
        setshowUserSelector(value)
    }

    const clickChatRoom = (target) => {
        setSelectedRoom({ name: target, type: chats[target].type })
        let oldData = { ...chats }
        oldData[target].unread = false
        setChats(oldData)
    }

    const startNewPrivateChat = (userTarget) => {
        let data = {
            ...chats,
            [userTarget]: {
                chats: [],
                type: 'PrivateChat',
                unread: false
            }
        }
        setChats(data)
        setSelectedRoom({ name: userTarget, type: 'PrivateChat' })
    }

    return (
        <div className='bg-blue-400 flex h-screen w-screen'>
            {showUserSelector ? (
                <UserSelector onlineUsers={onlineUsers} setshowUserSelector={setshowUserSelectorFunc}
                    startNewPrivateChat={startNewPrivateChat} username={username}/>
            ) : null}
            <div className='bg-blue-200 min-h-screen rounded-2xl p-2' style={{ width: '30%' }}>
                <div className='flex items-center border-b-2 border-white grow' style={{ height: '10%' }}>
                    <img className='' style={{ height: '50px', width: '50px' }} src="/logo.png" alt="" />
                    <div className='ml-2'>
                        <h1 className='font-extrabold text-sm text-white'>Forest Chat App</h1>
                        <h3 className='text-purple-500'>There are {onlineUsers.length} users online !</h3>
                    </div>
                </div>
                <div className='flex items-center content-center border-b-2 border-white grow' style={{ height: '10%' }}>
                    <div className='bg-blue-400 rounded-full flex items-center justify-center' style={{ width: '45px', height: '45px' }}>
                        <h1 className='text-white font-extrabold'>{inisial}</h1>
                    </div>
                    <div className='ml-5'>
                        <h1 className='font-bold'>{username}</h1>
                    </div>
                </div>
                <div className='flex items-center justify-evenly bg-slate-500 rounded-2xl mt-2' style={{ height: '7%' }}>
                    <h1 className='font-extrabold ml-2 text-2xl'>My Chats</h1>
                    <button className='bg-blue-400 rounded-full flex items-center justify-center' style={{ width: '120px', height: '25px' }}
                        onClick={(e) => setshowUserSelector(true)}>
                        <h1 className='font-bold text-lg'>+ new chat</h1>
                    </button>
                </div>
                <div className='bg-slate-400 rounded-2xl px-5 overflow-auto' style={{ height: '72%' }}>
                    {
                        Object.keys(chats).map((key, i) => {
                            // console.log('chats akhir')
                            // console.log(chats)
                            return (
                                <div className={selectedRoom.name == key ? 'w-full bg-slate-500 cursor-pointer p-2 rounded-md my-2' : 'w-full hover:bg-slate-500 cursor-pointer p-2 rounded-md my-2'}
                                    onClick={e => clickChatRoom(key)}>
                                    <div className='flex'>
                                        <h1 className='font-bold w-11/12'>{key}</h1>
                                        {
                                            (chats[key].unread === true) ?
                                                <div className='bg-white rounded-full w-3 h-3 mt-2'></div>
                                                :
                                                null
                                        }
                                    </div>
                                    {
                                        chats[key].chats.length === 0 ? null :
                                            <h1 className='text-sm text-slate-200'>
                                                {chats[key].chats[chats[key].chats.length - 1].username}
                                                :
                                                {chats[key].chats[chats[key].chats.length - 1].message.substring(0, 20)}
                                                {(chats[key].chats[chats[key].chats.length - 1].message.length > 20) ? "..." : null}
                                            </h1>
                                    }
                                </div>
                            )
                        })
                    }
                </div>
            </div>
            <div className='bg-blue-200 min-h-screen rounded-2xl p-2 mx-2' style={{ width: '70%' }}>
                <div className='bg-slate-500 rounded-2xl flex items-center justify-center' style={{ width: '100%', height: '10%' }}>
                    <h1 className='text-lg font-bold'>{selectedRoom.name}</h1>
                </div>
                <div className='bg-slate-400 rounded-2xl' style={{ width: '100%', height: '90%' }}>
                    <div className='p-2 flex flex-col justify-end overflow-y-auto' id='chatContent' style={{ height: '90%' }}>
                        <div className='h-full w-full' id='chatContainer'>
                            {/* {console.log(chats[`${selectedRoom.name}`])} */}
                            {chats[`${selectedRoom.name}`].chats.map((chat, id) => {
                                // console.log(chat)
                                if (chat.username !== username) {
                                    return (
                                        <div className='flex mb-4' key={id}>
                                            <div className='bg-blue-400 rounded-full flex items-center justify-center' style={{ width: '45px', height: '45px' }}>
                                                <h1 className='text-white font-extrabold'>{(chat.username[0] + chat.username[1]).toUpperCase()}</h1>
                                            </div>
                                            <div style={{ minWidth: '5%', width: 'fit-content', maxWidth: '70%', minWidth: '10%' }}>
                                                <div className='bg-slate-500 rounded-md p-1 ml-2'>
                                                    <h5 className='text-sm font-bold'>{chat.username}</h5>
                                                    <h1 className='' style={{ width: '100%', overflowWrap: 'break-word' }} dangerouslySetInnerHTML={{ __html: chat.message }}>
                                                        {/* {chat.message} */}
                                                    </h1>
                                                </div>
                                                <div className='flex justify-end pr-2' style={{ width: '100%' }}>
                                                    <h1 className='text-xs'>{timeManagement.getTimeDifferenceFromNow(parseInt(chat.time))}</h1>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }
                                else {
                                    return (
                                        <div className='flex flex-col items-end mb-4' style={{ width: '100%' }} key={id}>
                                            <div className='bg-blue-500 rounded-md p-1 px-3' style={{ minWidth: '5%', width: 'fit-content', maxWidth: '70%' }}>
                                                <h1 style={{ width: '100%', overflowWrap: 'break-word' }}>
                                                    {chat.message}
                                                </h1>
                                            </div>
                                            <div className='pr-2'>
                                                <h1 className='text-xs'>{timeManagement.getTimeDifferenceFromNow(parseInt(chat.time))}</h1>
                                            </div>
                                        </div>
                                    )
                                }
                            })}
                        </div>
                    </div>
                    <div className='flex items-center p-2' style={{ height: '10%' }}>
                        <form className='text-black' style={{ width: '100%' }} onSubmit={handleSubmit}>
                            <input className='rounded-md' style={{ width: '90%', padding: '5px' }} type="text" placeholder='enter a message'
                                onChange={(e) => setMessage(e.target.value)} value={message} />
                            <button className='rounded-md bg-blue-500 text-white ml-4' style={{ height: '30px', width: '60px' }}>Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}