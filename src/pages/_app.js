import '@/styles/globals.css'
import { onValue, push, ref, remove, set, update } from 'firebase/database'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { db } from '../firebase'

function App({ Component, pageProps }) {
  const [username, setUsername] = useState('')
  const [inisial,setInisial] = useState('')
  const [onlineUsers, setOnlineUsers] = useState([])
  const router = useRouter()

  const handleLogin = (e) => {
    e.preventDefault()
    if (onlineUsers.includes(username)) {
      alert('This username already used by other online user !')
      return router.push('/')
    }
    if (username === 'Public') {
      alert("Public can't be used as username , sorry :(")
      return router.push('/')
    }
    if(username.length <= 2){
      alert("Username must be at least 2 characters !")
      return router.push('/')
    }

    //console.log('INISIAL = ' + inisial)
    //insert current username into db
    set(ref(db, `/onlineUsers/${username}`), {
      username: username,
      status: 'online'
    })
    router.push('/chat')
  }

  useEffect(() => {
    if (username === '') {
      router.push('/')
    }

    //receive change on db onlineUsers       
    onValue(ref(db, `/onlineUsers`), (snapshot) => {
      if (snapshot.val()) {
        //console.log('online users changed !')
        //console.log(snapshot.val())
        let onlineUsersArr = []
        for (const username in snapshot.val()) {
          onlineUsersArr = [...onlineUsersArr, username]
        }
        //console.log(onlineUsersArr)
        setOnlineUsers(onlineUsersArr)
      }
    })

  }, [])

  const handleLoginChange = (e) => {
    setUsername(e.target.value)
    setInisial((e.target.value[0] + e.target.value[1]).toUpperCase())
  }

  return <Component handleLoginChange={handleLoginChange}
    username={username}
    inisial={inisial}
    handleLogin={handleLogin}
    onlineUsers={onlineUsers}
    {...pageProps} />
}

export default App
